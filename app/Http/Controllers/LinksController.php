<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LinksController extends Controller
{
    public  function saveLinks(Request $request){
        $request['user_id']=Auth::user()->id;
        $link=Links::create($request->all());
        return ['status'=>true,'message'=>'Link saved successfully'];
    }

    public  function deleteLink($id){
        $link=Links::find($id);
        $link->delete();
        return ['status'=>true,'message'=>'Link deleted successfully'];
    }

    public  function updateLinks(Request  $request){
        $link=Links::find($request->id);
        $link->update($request->all());
        return ['status'=>true,'message'=>'Link updated successfully'];
    }
}
