<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Subscription;
use App\Models\Subscription_log;
use App\Models\User;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public  function subscriptions_logs(){
        $user_id=Auth::user()->id;
        $subs=DB::select( DB::raw("SELECT *,
(SELECT phone from subscription_logs B WHERE B.id=A.subscription_log_id)phone,
(SELECT mpesa_ref from subscription_logs B WHERE B.id=A.subscription_log_id)mpesa_ref,
(SELECT amount from subscription_logs B WHERE B.id=A.subscription_log_id)amount,
(SELECT updated_at from subscription_logs B WHERE B.id=A.subscription_log_id)payment_date,
(SELECT method from subscription_logs B WHERE B.id=A.subscription_log_id)method
 FROM subscriptions A WHERE user_id='$user_id' ORDER BY id DESC"));
        return ['subs'=>$subs];
    }

    public  function donation_logs(){
        $user_id=Auth::user()->id;
        $logs=DB::select( DB::raw("SELECT * FROM subscription_logs WHERE user_id='$user_id' AND status='CONFIRMED' ORDER BY id DESC"));
        return ['logs'=>$logs];
    }
    public  function withdrawal_logs(){
        $user_id=Auth::user()->id;
        $logs=DB::select( DB::raw("SELECT * FROM withdrawal_logs WHERE user_id='$user_id'  ORDER BY id DESC"));
        return ['logs'=>$logs];
    }

    public  function getChatdata(){
        $user_id=Auth::user()->id;
        $year=date("Y");
        $data=DB::select( DB::raw("SELECT
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='1' AND user_id='$user_id' AND YEAR(updated_at)='$year')jan,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='2' AND user_id='$user_id' AND YEAR(updated_at)='$year')feb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='3' AND user_id='$user_id' AND YEAR(updated_at)='$year')mar,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='4' AND user_id='$user_id' AND YEAR(updated_at)='$year')apr,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='5' AND user_id='$user_id' AND YEAR(updated_at)='$year')may,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='6' AND user_id='$user_id' AND YEAR(updated_at)='$year')jun,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='7' AND user_id='$user_id' AND YEAR(updated_at)='$year')jul,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='8' AND user_id='$user_id' AND YEAR(updated_at)='$year')aug,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='9' AND user_id='$user_id' AND YEAR(updated_at)='$year')sep,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='10' AND user_id='$user_id' AND YEAR(updated_at)='$year')octb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='11' AND user_id='$user_id' AND YEAR(updated_at)='$year')nov,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='12' AND user_id='$user_id' AND YEAR(updated_at)='$year')dece
 FROM DUAL"));
        return ['data'=>$data];

    }

    public  function postContact(Request $request){
        $contact=Contact::create($request->all());
        return ['status'=>true,'message'=>'Information submitted successfully'];
    }

    public  function getAdminIndexData(){
        $year=date("Y");
        $donation=Subscription_log::where('status','CONFIRMED')->where('type','DONATION')->sum('amount');
        $withdraal=WithdrawalLogs::where('status','COMPLETED')->sum('amount');
        $admins=User::where('user_type','admin')->count();
        $artists=User::where('user_type','artist')->count();

        $data=DB::select( DB::raw("SELECT
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='1'  AND YEAR(updated_at)='$year')jan,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='2'  AND YEAR(updated_at)='$year')feb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='3'  AND YEAR(updated_at)='$year')mar,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='4'  AND YEAR(updated_at)='$year')apr,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='5' AND YEAR(updated_at)='$year')may,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='6'  AND YEAR(updated_at)='$year')jun,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='7'  AND YEAR(updated_at)='$year')jul,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='8'  AND YEAR(updated_at)='$year')aug,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='9'  AND YEAR(updated_at)='$year')sep,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='10'  AND YEAR(updated_at)='$year')octb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='11'  AND YEAR(updated_at)='$year')nov,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM subscription_logs WHERE STATUS='CONFIRMED' AND   MONTH(updated_at)='12'  AND YEAR(updated_at)='$year')dece
 FROM DUAL"));

        return ['donations'=>$donation,'admins'=>$admins,'artists'=>$artists,'info'=>$data,'withdrawal'=>$withdraal];
    }

    public  function getMpesaLogs(){

        $logs=DB::select( DB::raw("SELECT *,
       (SELECT user_name  from users B WHERE B.id=A.user_id)user_name,
       (SELECT uuid  from users B WHERE B.id=A.user_id)uuid
       FROM subscription_logs A WHERE status='CONFIRMED' order by id desc"));
        return ['logs'=>$logs];
    }

    public  function getWithdwalLogs(){
        $logs=DB::select( DB::raw("SELECT *,
       (SELECT user_name  from users B WHERE B.id=A.user_id)user_name,
       (SELECT uuid  from users B WHERE B.id=A.user_id)uuid
       FROM withdrawal_logs A WHERE status='COMPLETED' order by id desc"));
        return ['logs'=>$logs];
    }
}
