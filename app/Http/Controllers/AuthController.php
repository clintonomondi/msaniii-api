<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Deactivation;
use App\Models\Links;
use App\Models\Subscription;
use App\Models\Subscription_log;
use App\Models\User;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Knox\AFT\AFT;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        if(empty($request->email)){
            return ['status'=>false,'message'=>'Email  is required','active'=>'YES'];
        }
        if(empty($request->phone)){
            return ['status'=>false,'message'=>'Phone number  is required','active'=>'YES'];
        }
        if(empty($request->name)){
            return ['status'=>false,'message'=>'Name  is required','active'=>'YES'];
        }
        if(empty($request->user_name)){
            return ['status'=>false,'message'=>'Username  is required','active'=>'YES'];
        }
        if(empty($request->password)){
            return ['status'=>false,'message'=>'Password  is required','active'=>'YES'];
        }
        $request->validate([
            'email' => 'required|string|email',
            'phone' => 'required',
            'name' => 'required',
            'user_name' => 'required',
            'password' => 'required',
        ]);
        $credentials = request(['email', 'password']);
        $email=User::where('email',$request->email)->count();
        $active=User::where('email',$request->email)->where('status','DELETED')->where('user_type','artist')->count();
        $phone=User::where('phone',$request->phone)->count();
//        $sub_logs=Subscription_log::where('mpesa_ref',$request->mpesa_ref)->where('status','CONFIRMED')->where('type','SUBSCRIPTION')->count();
//        if($sub_logs<=0){
//            return ['status'=>false,'message'=>'Invalid M-pesa transaction code'];
//        }
        if($active>0){
            return ['status'=>false,'message'=>'Account already exist but inactive.','active'=>'NO'];
        }
        if($email>0){
            return ['status'=>false,'message'=>'Email address is already in use','active'=>'YES'];
        }
        if($phone>0){
            return ['status'=>false,'message'=>'The phone number  is already in use','active'=>'YES'];
        }
        $request['password']=bcrypt($request->password);
        $request['uuid']=Str::random(25);
        $user=User::create($request->all());
        $request['user_id']=$user->id;
//        $sub_logs_detail=Subscription_log::where('mpesa_ref',$request->mpesa_ref)->where('status','CONFIRMED')->where('type','SUBSCRIPTION')->first();
//        $request['subscription_log_id']=$sub_logs_detail->id;
//        $current =new Carbon($request->from_date);
//        $trialExpires = $current->addDays(30);
//        $request['to_date']=$trialExpires->format('Y-m-d H:i:s');
//        $subscribe=Subscription::create($request->all());
//        $data=Subscription_log::where('mpesa_ref',$request->mpesa_ref)->update(['status'=>'USED']);
        if(!empty($request->work_area)) {
            foreach ($request->work_area as $work) {
                $request['name']=$work;
                Area::create($request->all());
            }
        }
        if(!empty($request->links)) {
            foreach ($request->links as $link) {
                $request['title']=$link['title'];
                $request['url']=$link['url'];
                Links::create($request->all());
            }
        }

        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page','active'=>'YES'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addDays(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ];

    }

    public  function user(){
        $user=User::find(Auth::user()->id);
        $links=Links::orderBy('id','desc')->where('user_id',Auth::user()->id)->get();
//        $subscription=Subscription::where('user_id',Auth::user()->id)->where('status','ACTIVE')->first();
//        $sub_status=Subscription::where('user_id',Auth::user()->id)->where('status','ACTIVE')->count();
        $withdrawn=WithdrawalLogs::where('user_id',Auth::user()->id)->sum('amount');
        $donate=Subscription_log::where('user_id',Auth::user()->id)->where('status','CONFIRMED')->sum('amount');
        return ['user'=>$user,'links'=>$links,'withdrawn'=>$withdrawn,'donation'=>$donate];

    }

    public  function updatePhoto(Request $request){
        try {
            $user=User::find(Auth::user()->id);
            if(!empty($request['file'])) {
                $data = substr($request->file, strpos($request->file, ',') + 1);
                $file = base64_decode($data);
                $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'png';
                Storage::disk('public')->put('Photos/' . $fileNameToStore, $file);

                if($request->action=='background'){
                    Storage::delete('/public/Photos/'.$user->photo_url);
                    $request['bg_url'] = $fileNameToStore;
                }else{
                    Storage::delete('/public/Photos/'.$user->photo_url);
                    $request['photo_url'] = $fileNameToStore;
                }
                $user->update($request->all());
            }
            return ['status'=>true,'message'=>'Process completed successfully'];
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);

        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }



    public  function updateProfileInfo(Request $request){
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Information updated successfully'];
    }

    public  function updateProfilePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='YES';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Password updated successfully'];
    }

    public  function forget(Request $request){
        $data=User::where('email',$request->email)->count();
        if($data<=0){
            return ['status'=>false,'message'=>'The email you entered does not exist'];
        }
        $user=User::where('email',$request->email)->first();
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';


        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='Your msaniii new password is  '.$code;
        AFT::sendMessage($phone, $message);

        $d=User::where('email',$request->email)->update(['password'=>$request->password,'password_changed'=>$request->password_changed]);
        return ['status'=>true,'user'=>$user,'message'=>'A new password has been sent to your phone,enter the password'];

    }

    public  function changeUserStatus(Request $request,$id){
        if($id==Auth::user()->id){
            return ['status'=>false,'message'=>'You are not authorised to deactivate your account'];
        }
        $user=User::find($id);
        if($request->status=='INACTIVE'){
            if(empty($request->reason)){
                return ['status'=>false,'message'=>'Please provide reason for deactivating this user'];
            }
            $request['user_id']=$id;
            Deactivation::create($request->all());
        }
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }
    public  function updateUser(Request $request,$id){
        $user=User::find($id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }

    public  function addUser(Request $request){
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email address is already in use'];
        }
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';
        $request['user_name']='NO';
        $request['user_type']='admin';
        $request['uuid']=Str::random(25);
        if(strlen($request->phone)==10){
            $phone=$request->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($request->phone,4));
        }
        $message='You been on boarded on msaniii as admin. Your password is  '.$code;
        AFT::sendMessage($phone, $message);
        $user=User::create($request->all());

        return ['status'=>true,'message'=>'User added successfully'];

    }

    public  function deleteAccount(){
        $user=User::find(Auth::user()->id);
        $user->update(['status'=>'DELETED']);
        return ['status'=>true,'message'=>'Account deleted successfully '];
    }

    public  function activateAccount($id){
        $user=User::where('email',$id)->first();
        $u=User::find($user->id);
        $u->update(['status'=>'ACTIVE']);
        return ['status'=>true,'message'=>'Account activated  successfully.Please signin'];
    }

}
