<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Links;
use App\Models\Subscription;
use App\Models\Subscription_log;
use App\Models\User;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtistsController extends Controller
{
    public  function artists(){
        $artists=DB::select( DB::raw("SELECT * FROM users WHERE user_type='artist' AND status='ACTIVE'  order by user_name ASC"));
        return ['artists'=>$artists];
    }
    public  function artist2($id){
        $artist=User::where('uuid',$id)->first();
        $donation=Subscription_log::where('user_id',$artist->id)->where('status','CONFIRMED')->sum('amount');
        $links=Links::where('user_id',$artist->id)->get();
        $area=Area::select('name')->where('user_id',$artist->id)->get();
        $withdraw=WithdrawalLogs::where('user_id',$artist->id)->where('status','COMPLETED')->sum('amount');
        return['artist'=>$artist,'links'=>$links,'donations'=>$donation,'areas'=>$area,'withdraw'=>$withdraw];
    }
    public  function artist($id){
        $artist=User::where('uuid',$id)->first();
        $links=Links::where('user_id',$artist->id)->get();
        return['artist'=>$artist,'links'=>$links];
    }

    public  function searchArtistRandomly($id){
        $artists=DB::select( DB::raw("SELECT * FROM users WHERE user_name LIKE  '%$id%' AND  user_type='artist' AND status='ACTIVE'  order by user_name ASC"));
        return ['artists'=>$artists];
    }
    public  function getAllArtists(){
        $artists=DB::select( DB::raw("SELECT *,
(SELECT sum(amount) FROM subscription_logs B WHERE A.id=B.user_id AND status='CONFIRMED' AND type='DONATION')donations
 FROM users A WHERE user_type='artist'  order by user_name ASC"));
        return ['artists'=>$artists];
    }

    public  function getUsers(){
        $artists=DB::select( DB::raw("SELECT *,
(SELECT sum(amount) FROM subscription_logs B WHERE A.id=B.user_id AND status='CONFIRMED' AND type='DONATION')donations
 FROM users A WHERE user_type='admin'  order by name ASC"));
        return ['artists'=>$artists];
    }
}
