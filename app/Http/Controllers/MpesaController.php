<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\Subscription_log;
use App\Models\User;
use App\Models\Wallet;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Knox\AFT\AFT;
use MPESA;

class MpesaController extends Controller
{
//    public  function initiateSubscriptionSTK(Request $request){
//        try {
//
//            if(strlen($request->phone2)==10){
//                $phone='254'.substr($request->phone2,1);
//            }else{
//                $phone=str_replace(' ','','254'.substr($request->phone2,4));
//            }
//            $mpesa = MPESA::stkPush((int)$phone,(int)$request->amount,$phone);
//
//            $request['system_ref']='MS'.mt_rand(10000,99999);
//            $request['MerchantRequestID']=$mpesa->MerchantRequestID;
//            $request['CheckoutRequestID']=$mpesa->CheckoutRequestID;
//            $request['amount']=$request->amount;
//            $request['phone']=$phone;
//            $request['account']=$phone;
//            $request['ResponseCode']=$mpesa->ResponseCode;
//            $request['ResponseDescription']=$mpesa->ResponseDescription;
//            $data=Subscription_log::create($request->all());
//
//            return ['status'=>true,'message'=>'STK pushed initiated, please enter PIN','ResponseCode'=>$mpesa->ResponseCode,'ResponseMessage'=>$mpesa->CustomerMessage,
//                'MerchantRequestID'=>$request->MerchantRequestID,'CheckoutRequestID'=>$request->CheckoutRequestID];
//
//        } catch (\Exception $e) {
//            return ['status'=>false,'ResponseMessage'=>$e->getMessage()];
//        }
//    }
//    public  function initiateSubscriptionSTK2(Request $request){
//        try {
//
//            if(strlen($request->phone2)==10){
//                $phone='254'.substr($request->phone2,1);
//            }else{
//                $phone=str_replace(' ','','254'.substr($request->phone2,4));
//            }
//            $mpesa = MPESA::stkPush((int)$phone,(int)$request->amount,Auth::user()->email);
//
//            $request['system_ref']='MS'.mt_rand(10000,99999);
//            $request['MerchantRequestID']=$mpesa->MerchantRequestID;
//            $request['CheckoutRequestID']=$mpesa->CheckoutRequestID;
//            $request['amount']=$request->amount;
//            $request['phone']=$phone;
//            $request['account']=Auth::user()->email;
//            $request['ResponseCode']=$mpesa->ResponseCode;
//            $request['ResponseDescription']=$mpesa->ResponseDescription;
//            $request['user_id']=Auth::user()->id;
//            $data=Subscription_log::create($request->all());
//            return ['status'=>true,'message'=>'STK pushed initiated, please enter PIN','ResponseCode'=>$mpesa->ResponseCode,'ResponseMessage'=>$mpesa->CustomerMessage,
//                'MerchantRequestID'=>$request->MerchantRequestID,'CheckoutRequestID'=>$request->CheckoutRequestID];
//
//        } catch (\Exception $e) {
//            return ['status'=>false,'ResponseMessage'=>$e->getMessage()];
//        }
//    }
    public  function initiateDonationSTK(Request $request){
        try {
            $artist=User::where('uuid',$request->uuid)->first();
            $phone_captured=str_replace(' ', '', $request->phone2);
            if(strlen($phone_captured)==10){
                $phone=str_replace(' ','','254'.substr($phone_captured,1));
            }else{
                $phone=str_replace(' ','','254'.substr($phone_captured,4));
            }
            $mpesa = MPESA::stkPush((int)$phone,(int)$request->amount,$artist->user_name);

            $request['system_ref']='MS'.mt_rand(10000,99999);
            $request['MerchantRequestID']=$mpesa->MerchantRequestID;
            $request['CheckoutRequestID']=$mpesa->CheckoutRequestID;
            $request['amount']=$request->amount;
            $request['phone']=$phone;
            $request['account']=$artist->user_name;
            $request['ResponseCode']=$mpesa->ResponseCode;
            $request['ResponseDescription']=$mpesa->ResponseDescription;
            $request['type']='DONATION';
            $request['user_id']=$artist->id;
            $data=Subscription_log::create($request->all());

            return ['status'=>true,'message'=>'STK pushed initiated, please enter PIN','ResponseCode'=>$mpesa->ResponseCode,'ResponseMessage'=>$mpesa->CustomerMessage,
                'MerchantRequestID'=>$request->MerchantRequestID,'CheckoutRequestID'=>$request->CheckoutRequestID];

        } catch (\Exception $e) {
            return ['status'=>false,'ResponseMessage'=>$e->getMessage()];
        }
    }

    public  function checkSubscriptionStkPayment(Request $request){
        $info=Subscription_log::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
        if($info->status=='CONFIRMED'){
            return ['status'=>true,'mpesa_ref'=>$info->mpesa_ref,'callback'=>true];
        }else if($info->status=='CANCELLED'){
            return ['status'=>true,'message'=>'User cancelled payment.Pleaese wait.....','callback'=>false];
        }
        else{
            return ['status'=>false];
        }
    }

    public  function callBack(Request $request){
        try {
            Log::info('Calling back STK');
            $response = $request->all();
            Log::info($response['Body']['stkCallback']);
            $request['ResultCode'] = $response['Body']['stkCallback']['ResultCode'];
            $request['ResultDesc'] = $response['Body']['stkCallback']['ResultDesc'];
            $request['MerchantRequestID'] = $response['Body']['stkCallback']['MerchantRequestID'];
            $request['CheckoutRequestID'] = $response['Body']['stkCallback']['CheckoutRequestID'];
            if ($request->ResultCode == '0') {
                $collection = collect($response['Body']['stkCallback']['CallbackMetadata']['Item']);
                $transaction = $collection->where('Name', 'MpesaReceiptNumber')->first();
                $info=Subscription_log::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=Subscription_log::find($info->id);
                $request['status']='CONFIRMED';
                $request['mpesa_ref']=$transaction['Value'];
                $request['trans_id']=$transaction['Value'];
                $request['payment_ref']=$transaction['Value'];
                $request['system_id']=$logs->system_ref;
                $request['payment_method']='M-PESA EXPRESS';
                $logs->update($request->all());

//                if($logs->type=='DONATION'){
                    $artist=User::find($logs->user_id);
                    $new_balance=$artist->balance+$logs->amount;
                    $data=User::where('id',$artist->id)->update(['balance'=>$new_balance]);

                    if(strlen($logs->phone)==10){
                        $phone=$logs->phone;
                    }else{
                        $phone=str_replace(' ','','0'.substr($logs->phone,3));
                    }
                    $message='You have  sent Ksh.  '.$logs->amount.' to '.$artist->user_name.',M-Pesa id '.$request->mpesa_ref.' reference '.$logs->system_ref.'.@MSANIII';
                    AFT::sendMessage($phone, $message);
//                }else{
//                   if(!empty($logs->user_id)) {
//                       $request['subscription_log_id'] = $logs->id;
//                       $request['from_date']=$logs->created_at;
//                       $current = new Carbon($request->from_date);
//                       $trialExpires = $current->addDays(30);
//                       $request['to_date'] = $trialExpires->format('Y-m-d H:i:s');
//                       $request['status'] = 'ACTIVE';
//                       $request['user_id'] = $logs->user_id;
//                       $subscribe = Subscription::create($request->all());
//                   }
//
//                }

            }else{
                $info=Subscription_log::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=Subscription_log::find($info->id);
                $request['status']='CANCELLED';
                $logs->update($request->all());
           }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }
    public  function b2cResult(Request $request){
        try {
            Log::info('Calling back B2C oooooooooooooh');
            Log::info($request);
            $request['ResultCode']=$request['Result']['ResultCode'];
            if ($request->ResultCode == '0') {
                $request['ConversationID']=$request['Result']['ConversationID'];
                $request['OriginatorConversationID']=$request['Result']['OriginatorConversationID'];
                $request['ResponseDescription']=$request['Result']['ResultDesc'];
                $request['TransactionID']=$request['Result']['TransactionID'];
//                $request['balance']=$request['Result']['ResultParameters']['ResultParameter'][4]['Value'];
                $request['date']=$request['Result']['ResultParameters']['ResultParameter'][3]['Value'];
                $request['user']=$request['Result']['ResultParameters']['ResultParameter'][2]['Value'];
                $request['status']='COMPLETED';

                $d=WithdrawalLogs::where('ConversationID',$request->ConversationID)->where('OriginatorConversationID',$request->OriginatorConversationID)->first();
                $data=WithdrawalLogs::find($d->id);
                $data->update($request->all());
                $user=User::find($data->user_id);

                if(strlen($user->phone)==10){
                    $phone=$user->phone;
                }else{
                    $phone=str_replace(' ','','0'.substr($user->phone,4));
                }
                $message='You have withdrawn Ksh.'.$data->amount.' from MSANIII.M-Pesa REF: '.$request->TransactionID.' . System REF: '.$data->system_ref.' Balance Ksh.'.$user->balance;
                AFT::sendMessage($phone, $message);
            }

        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
        return ['status'=>true];
    }

    public function registerURL(Request $request){
        $mpesa = MPESA::registerC2bUrl();
        Log::info(json_encode($mpesa));
        return ['mpesa'=>$mpesa];
    }
}
