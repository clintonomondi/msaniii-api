<?php

namespace App\Http\Controllers;

use App\Models\Deduction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public  function addDeduction(Request $request){
        $validated = $request->validate([
            'percent' => 'required|numeric',
        ]);
        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $data=Deduction::create($request->all());
        return ['status'=>true,'message'=>'deduction added successfully'];
    }

    public  function getDeductions(){
        $deductions=Deduction::all();
        return ['deductions'=>$deductions];
    }

    public  function updateDeduction(Request $request,$id){
        $validated = $request->validate([
            'percent' => 'required|numeric',
        ]);
        $d=Deduction::find($id);
        $d->update($request->all());
        return ['status'=>true,'message'=>'deduction updated successfully'];
    }

    public  function deleteDeduction($id){
        $d=Deduction::find($id);
        $d->delete();
        return ['status'=>true,'message'=>'deduction removed successfully'];
    }
}
