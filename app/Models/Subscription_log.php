<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription_log extends Model
{
    use HasFactory;
    protected $fillable=['mpesa_ref','system_ref','MerchantRequestID','CheckoutRequestID','amount','account','ResponseCode','ResponseDescription','status','phone',
        'ResultCode','ResultDesc','method','user_id','type'];
}
