<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('forget', 'App\Http\Controllers\AuthController@forget');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::get('artists', 'App\Http\Controllers\ArtistsController@artists');
    Route::get('searchArtistRandomly/{id}', 'App\Http\Controllers\ArtistsController@searchArtistRandomly');
    Route::post('contact', 'App\Http\Controllers\LogsController@postContact');
    Route::get('getDeductions', 'App\Http\Controllers\SettingsController@getDeductions');
    Route::get('artist/{id}', 'App\Http\Controllers\ArtistsController@artist');
//Mpesa
    Route::post('initiateSubscriptionSTK', 'App\Http\Controllers\MpesaController@initiateSubscriptionSTK');
    Route::post('initiateDonationSTK', 'App\Http\Controllers\MpesaController@initiateDonationSTK');
    Route::post('checkSubscriptionStkPayment', 'App\Http\Controllers\MpesaController@checkSubscriptionStkPayment');
    Route::post('callBack', 'App\Http\Controllers\MpesaController@callBack');
    Route::post('c2bConfirm', 'App\Http\Controllers\MpesaController@c2bConfirm');
    Route::post('registerURL', 'App\Http\Controllers\MpesaController@registerURL');

    Route::post('b2cResult', 'App\Http\Controllers\MpesaController@b2cResult');
    Route::get('activateAccount/{id}', 'App\Http\Controllers\AuthController@activateAccount');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('artist2/{id}', 'App\Http\Controllers\ArtistsController@artist2');
        Route::post('initiateSubscriptionSTK2', 'App\Http\Controllers\MpesaController@initiateSubscriptionSTK2');
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');
        Route::post('updatePhoto', 'App\Http\Controllers\AuthController@updatePhoto');
        Route::post('updateProfileInfo', 'App\Http\Controllers\AuthController@updateProfileInfo');
        Route::post('updateProfilePassword', 'App\Http\Controllers\AuthController@updateProfilePassword');

        //
        Route::post('saveLinks', 'App\Http\Controllers\LinksController@saveLinks');
        Route::post('updateLinks', 'App\Http\Controllers\LinksController@updateLinks');
        Route::get('deleteLink/{id}', 'App\Http\Controllers\LinksController@deleteLink');
        Route::get('subscriptions_logs', 'App\Http\Controllers\LogsController@subscriptions_logs');
        Route::get('withdrawal_logs', 'App\Http\Controllers\LogsController@withdrawal_logs');
        Route::get('donation_logs', 'App\Http\Controllers\LogsController@donation_logs');
        Route::get('getChatdata', 'App\Http\Controllers\LogsController@getChatdata');


        Route::post('getWithdrawalCode', 'App\Http\Controllers\OtpController@getWithdrawalCode');
        Route::get('deleteAccount', 'App\Http\Controllers\AuthController@deleteAccount');


        ///////ADMIN RESOURCES
        //Index
        Route::get('getAdminIndexData', 'App\Http\Controllers\LogsController@getAdminIndexData');
        Route::get('getAllArtists', 'App\Http\Controllers\ArtistsController@getAllArtists');
        Route::get('getUsers', 'App\Http\Controllers\ArtistsController@getUsers');
        Route::post('changeUserStatus/{id}', 'App\Http\Controllers\AuthController@changeUserStatus');
        Route::post('updateUser/{id}', 'App\Http\Controllers\AuthController@updateUser');
        Route::post('addUser', 'App\Http\Controllers\AuthController@addUser');
        Route::post('addDeduction', 'App\Http\Controllers\SettingsController@addDeduction');
        Route::post('updateDeduction/{id}', 'App\Http\Controllers\SettingsController@updateDeduction');
        Route::get('deleteDeduction/{id}', 'App\Http\Controllers\SettingsController@deleteDeduction');
        Route::get('getMpesaLogs', 'App\Http\Controllers\LogsController@getMpesaLogs');
        Route::get('getWithdwalLogs', 'App\Http\Controllers\LogsController@getWithdwalLogs');
    });
});
